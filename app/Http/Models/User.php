<?php

namespace App\Http\Models;


use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;


use Auth;
use Hash;


class User extends Model implements
    AuthenticatableContract,
    AuthorizableContract
{
     use Authenticatable, Authorizable;
	 
	 
	protected $table = 'users';
    protected $fillable = ['userID','login', 'password','admin','api_token','api_token_expired'];
	public  $timestamps = false;
	protected $primaryKey = 'userID';
	
	private $expiredToken = 2*60*60;
	
	
	public function attemptLogin($request){
		$user = User::where('login', $request->json()->get('login'))->get();
		if(count($user) > 0)
		{
			if (app('hash')->check($request->json()->get('password'), $user[0]->password))
			{
				$user[0]->api_token = str_random(255);
				$user[0]->api_token_expired = $this->expiredToken+time();
				$user[0]->save();
				return ['api_token'=>$user[0]->api_token];
			}else throw new \Exception('Niepoprawne hasło');
		}else throw new \Exception('Niepoprawny login:'.$request->input('login'));
	}
	
	
	public function changeAdminPassword($data){
		if($data['password'] === $data['passwordcf']){	
			$user = User::find(1);
			if(!empty($user)){
				if (app('hash')->check($data['passwordcr'], $user->password)){
					$user->password = app('hash')->make($data['password']);
					$user->save();
					return json_encode(['message'=>'Hasło zmienione!']);
				}else throw new \Exception('Niepoprawne hasło');			
			}	
		}else throw new \Exception('Hasła się nie zgadzają');
	}
	
}
