<?php

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

	protected $table = 'categories';
    protected $fillable = ['categoryID','title', 'alias'];
	public  $timestamps = false;
	protected $primaryKey = 'categoryID';
}
