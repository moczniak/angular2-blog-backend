<?php

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class Page extends Model
{

	protected $table = 'pages';
    protected $fillable = ['pageID','title', 'alias', 'content', 'publishDate', 'publishDateStamp', 'published', 'visited','seoTitle','seoDesc','seoKeywords'];
	public  $timestamps = false;
	protected $primaryKey = 'pageID';
	
}
