<?php

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class Post extends Model
{

	protected $table = 'posts';
    protected $fillable = ['postID','title', 'alias', 'categoryID', 'content', 'publishDate', 'publishDateStamp', 'promoImage', 'published', 'visited','seoTitle','seoDesc','seoKeywords'];
	public  $timestamps = false;
	protected $primaryKey = 'postID';
	
	public function category(){
		return $this->hasOne('App\Http\Models\Category', 'categoryID', 'categoryID');
	}
	
	
	
	
	public function base64ToImage($base64_string){
		$random = str_random(10);
		$path = $_SERVER['DOCUMENT_ROOT'].'/api/resources/upload/'.$random.'.jpg';
		$ifp = fopen($path, "wb"); 

		$data = explode(',', $base64_string);

		fwrite($ifp, base64_decode($data[1])); 
		fclose($ifp); 

		return 'resources/upload/'.$random.'.jpg'; 	
	}
	
	
	public function removeFile($filePath){
		$path = $_SERVER['DOCUMENT_ROOT'].'/api/'.$filePath;
		try{
			unlink($path);
		}catch(\Exception $e){}
	}
}
