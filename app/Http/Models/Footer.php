<?php

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class Footer extends Model
{

	protected $table = 'footers';
    protected $fillable = ['footerID','content'];
	public  $timestamps = false;
	protected $primaryKey = 'footerID';
}
