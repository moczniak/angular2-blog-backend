<?php

namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{

	protected $table = 'settings';
    protected $fillable = ['settingID','title', 'description','keywords','siteTitle'];
	public  $timestamps = false;
	protected $primaryKey = 'settingID';
}
