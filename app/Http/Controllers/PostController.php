<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

use App\Http\Models\Post;

use Illuminate\Support\Facades\Input;

class PostController extends Controller
{
	
	public function allFromCategory($categoryID){
		return Post::where('categoryID', $categoryID)->with('category')->get();
	}
	
	
    public function all(){
		return Post::with('category')->get();
	}
	
	
	public function categoryPage($categoryID, $page, Request $request){
		$limit = $request->input('limit');
		if(empty($limit)) $limit = 10;
		return Post::where('categoryID',$categoryID)->with('category')->skip($limit*($page-1))->take($limit)->get();	
	}
	
	public function page($page, Request $request){
		$limit = $request->input('limit');
		if(empty($limit)) $limit = 10;
		return Post::with('category')->skip($limit*($page-1))->take($limit)->get();
	}
	
	public function popular(){
		return Post::orderBy('visited', 'desc')->take(5)->get();
	}
	
	public function FromCategoryCount($categoryID){
		return ['count'=>Post::where('categoryID',$categoryID)->count()];
	}
	
	public function count(){
		return ['count'=>Post::count()];
	}
	
	public function create(Request $request){
		$data = $request->json()->all();
		$post = new Post();
		$post->title = $data['post']['title'];
		$post->content = $data['post']['content'];
		$post->seoTitle = $data['post']['seoTitle'];
		$post->seoDesc = $data['post']['seoDesc'];
		$post->seoKeywords = $data['post']['seoKeywords'];
		
		if(!empty($data['post']['categoryID'])){
			$post->categoryID = $data['post']['categoryID'];
		}
		$post->publishDate = date('Y-m-d H:i:s');
		if(empty($data['post']['promoImage'])){
			$post->promoImage = '';
		}else{
			$post->promoImage = $post->base64ToImage($data['post']['promoImage']);
		}
		$post->published = 1;
		$post->alias = $data['post']['alias'];
		$post->save();
		return $post;
	}
	
	public function update($postID, Request $request){
		$post = Post::find($postID);
		$data = $request->json()->all();
		$post->title = $data['post']['title'];
		$post->content = $data['post']['content'];
		$post->seoTitle = $data['post']['seoTitle'];
		$post->seoDesc = $data['post']['seoDesc'];
		$post->seoKeywords = $data['post']['seoKeywords'];
		if(!empty($data['post']['categoryID'])){
			$post->categoryID = $data['post']['categoryID'];
		}
		$post->publishDate = date('Y-m-d H:i:s');
		if(empty($data['post']['promoImage'])){
			if(!empty($post->promoImage)){
				$post->removeFile($post->promoImage);
				$post->promoImage = '';
			}
		}else{
			if(!empty($post->promoImage)){
				if('http://'.$_SERVER['SERVER_NAME'].'/api/'.$post->promoImage != $data['post']['promoImage']){
					$post->removeFile($post->promoImage);
					$post->promoImage = '';
					$post->promoImage = $post->base64ToImage($data['post']['promoImage']);
				}
			}else{
				$post->promoImage = $post->base64ToImage($data['post']['promoImage']);
			}
		}
		$post->published = 1;
		$post->alias = $data['post']['alias'];
		$post->save();
		//nn
		return $post;
	}
	
	public function delete($postID){
		$post = Post::where('postID',$postID)->delete();
		return $post;
	}
	
	
	public function detail($postID){
		$post = Post::where('postID',$postID)->with('category')->take(1)->get();
		$post[0]->visited += 1;
		$post[0]->save();
		return $post[0];
	}
	
}
