<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

use App\Http\Models\Setting;

class SettingController extends Controller
{
    public function get(){
		return Setting::where('settingID',1)->take(1)->get()[0];
	}
	
	public function update(Request $request){
		$data = $request->json()->all()['setting'];
		$setting = Setting::find(1);
		$setting->title = $data['title'];
		$setting->description = $data['description'];
		$setting->keywords = $data['keywords'];
		$setting->siteTitle = $data['siteTitle'];
		$setting->save();
		return $setting;
		
	}
	
	
	
	
}
