<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

use App\Http\Models\Footer;

class FooterController extends Controller
{
    public function all(){
		return Footer::all();
	}
	
}
