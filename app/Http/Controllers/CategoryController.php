<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

use App\Http\Models\Category;
use App\Http\Models\Post;

class CategoryController extends Controller
{
    public function all(){
		return Category::all();
	}
	
	
	public function create(Request $request){
		$data = $request->json()->all()['category'];
		$category = new Category();
		$category->title = $data['title'];
		$category->alias = $data['alias'];
		$category->save();
		return $category;
	}
	
	public function delete($categoryID){
		$category = Category::where('categoryID',$categoryID)->delete();
		Post::where('categoryID',$categoryID)->update(['categoryID' => '']);
		return $category;
	}
	
	public function update($categoryID, Request $request){
		$category = Category::find($categoryID);
		if(count($category) > 0){
			$data = $request->json()->all()['category'];
			$category->title = $data['title'];
			$category->alias = $data['alias'];
			$category->save();
			return $category;
		}
	}
	
	
}
