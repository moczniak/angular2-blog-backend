<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

use App\Http\Models\User;

use Illuminate\Http\Response;



class UserController extends Controller
{	
	
	/*
	public function test(){
		$user = new User();
		$user->login = 'admin';
		$user->password = app('hash')->make('admin');
		$user->admin = 1;
		$user->save();
		return $user;
	}
	*/
	
	
	public function changeAdminPSW(Request $request){
		$data = $request->json()->all()['passwords'];
		$user = new User();
		try{
			return $user->changeAdminPassword($data);
		}catch(\Exception $e){
			return response()->json(['message' => $e->getMessage()],521);
		}
	}
	
	
    public function login(Request $request){
		$user = new User();
		try{
			return $user->attemptLogin($request);
		}catch(\Exception $e){
			return response()->json(['message' => $e->getMessage()],521);
		}
	}

}
