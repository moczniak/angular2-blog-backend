<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

use App\Http\Models\Page;

use Illuminate\Support\Facades\Input;

class PageController extends Controller
{
	
	
	
    public function all(){
		return Page::all();
	}
	
	
	public function page($page, Request $request){
		$limit = $request->input('limit');
		if(empty($limit)) $limit = 10;
		return Page::skip($limit*($page-1))->take($limit)->get();
	}
	
	
	public function count(){
		return ['count'=>Page::count()];
	}
	
	public function create(Request $request){
		$data = $request->json()->all()['page'];
		$page = new Page();
		$page->title = $data['title'];
		$page->content = $data['content'];
		$page->seoTitle = $data['seoTitle'];
		$page->seoDesc = $data['seoDesc'];
		$page->seoKeywords = $data['seoKeywords'];
		$page->publishDate = date('Y-m-d H:i:s');
		$page->publishDateStamp = time();
		$page->published = 1;
		$page->alias = $data['alias'];
		$page->save();
		return $page;
	}
	
	public function update($pageID, Request $request){
		$page = Page::find($pageID);
		$data = $request->json()->all()['page'];
		$page->title = $data['title'];
		$page->content = $data['content'];
		$page->seoTitle = $data['seoTitle'];
		$page->seoDesc = $data['seoDesc'];
		$page->seoKeywords = $data['seoKeywords'];
		$page->publishDate = date('Y-m-d H:i:s');
		$page->publishDateStamp = time();
		$page->published = 1;
		$page->alias = $data['alias'];
		$page->save();
		//nn
		return $page;
	}
	
	public function delete($pageID){
		$page = Page::where('pageID',$pageID)->delete();
		return $page;
	}
	
	
	public function detail($pageID){
		$page = Page::where('pageID',$pageID)->take(1)->get();
		$page[0]->visited += 1;
		$page[0]->save();
		return $page[0];
	}
	
}
