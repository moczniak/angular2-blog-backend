<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
//$app->get('/', 'UserController@test');



$app->group(['middleware' => 'cors','namespace' => 'App\Http\Controllers'], function () use ($app) {
	
	$app->get('/setting', 'SettingController@get');
	
	
	$app->options('/user/login', function () { return 'ok'; });
	$app->put('/user/login', 'UserController@login');
	
	
	$app->get('/post/popular', 'PostController@popular');
	$app->get('/post/count', 'PostController@count');
	$app->get('/post/category/{categoryID}/count', 'PostController@FromCategoryCount');
	$app->get('/post/category/{categoryID}', 'PostController@allFromCategory');
	$app->get('/post', 'PostController@all');
	$app->get('/post/{postID}', 'PostController@detail');
	$app->get('/post/page/{page}', 'PostController@page');
	$app->get('/post/category/{categoryID}/page/{page}', 'PostController@categoryPage');
	
	
	$app->get('category', 'CategoryController@all');
	
	
	$app->get('/page/count', 'PageController@count');
	$app->get('/page', 'PageController@all');
	$app->get('/page/{pageID}', 'PageController@detail');
	$app->get('/page/page/{page}', 'PageController@page');
	
	$app->get('/footer', 'FooterController@all');
	
});

$app->group(['middleware' =>  ['cors', 'auth'],'namespace' => 'App\Http\Controllers'], function () use ($app) {
	
	$app->get('/tryAuth', function(){ return ['message'=>'ok']; });
	$app->post('/post', 'PostController@create');
	
	$app->options('/post/{postID}', function () { return 'ok'; });
	$app->put('/post/{postID}', 'PostController@update');
	$app->delete('/post/{postID}', 'PostController@delete');
	
	
	$app->post('/category', 'CategoryController@create');
	$app->options('/category/{categoryID}', function () { return 'ok'; });
	$app->delete('/category/{categoryID}', 'CategoryController@delete');
	$app->put('/category/{categoryID}', 'CategoryController@update');
	
	
	
	$app->options('/setting', function () { return 'ok'; });
	$app->put('/setting', 'SettingController@update');
	
	
	
	$app->post('/page', 'PageController@create');
	$app->options('/page/{pageID}', function () { return 'ok'; });
	$app->put('/page/{pageID}', 'PageController@update');
	$app->delete('/page/{pageID}', 'PageController@delete');
	
	
	$app->options('/user', function () { return 'ok'; });
	$app->put('/user', 'UserController@changeAdminPSW');
	

	
});