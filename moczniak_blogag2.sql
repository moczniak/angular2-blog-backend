-- phpMyAdmin SQL Dump
-- version 3.4.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Czas wygenerowania: 06 Cze 2016, 14:38
-- Wersja serwera: 5.5.47
-- Wersja PHP: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Baza danych: `moczniak_blogag2`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `categoryID` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`categoryID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=4 ;

--
-- Zrzut danych tabeli `categories`
--

INSERT INTO `categories` (`categoryID`, `title`, `alias`) VALUES
(1, 'Ja', 'ja'),
(2, 'Otoczenie', 'otoczenie'),
(3, 'Filozofowanie', 'filozofowanie');

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `footers`
--

CREATE TABLE IF NOT EXISTS `footers` (
  `footerID` int(11) NOT NULL AUTO_INCREMENT,
  `content` text NOT NULL,
  PRIMARY KEY (`footerID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin2 AUTO_INCREMENT=4 ;

--
-- Zrzut danych tabeli `footers`
--

INSERT INTO `footers` (`footerID`, `content`) VALUES
(1, '["page",{"pageID":2}]'),
(2, '<h2>Reklama</h2><img src="https://images.alphacoders.com/549/549919.jpg" class="img-responsive">'),
(3, '<h2>FuckBook</h2>');

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `pageID` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `publishDate` varchar(255) NOT NULL,
  `publishDateStamp` int(11) NOT NULL,
  `visited` int(11) NOT NULL,
  `published` int(11) NOT NULL,
  `seoTitle` varchar(255) NOT NULL,
  `seoDesc` varchar(255) NOT NULL,
  `seoKeywords` varchar(255) NOT NULL,
  PRIMARY KEY (`pageID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin2 AUTO_INCREMENT=3 ;

--
-- Zrzut danych tabeli `pages`
--

INSERT INTO `pages` (`pageID`, `title`, `alias`, `content`, `publishDate`, `publishDateStamp`, `visited`, `published`, `seoTitle`, `seoDesc`, `seoKeywords`) VALUES
(2, 'O mnie', 'o-mnie', '<h2>O mnie</h2>\n\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris sagittis vulputate ipsum sollicitudin varius. Aenean dapibus congue est eu condimentum. Ut vestibulum gravida sem vitae convallis. Vestibulum nisi lectus, condimentum et scelerisque ac, tincidunt vestibulum dolor. In elementum tellus vitae sem mattis, ac consequat lectus sollicitudin. Proin ac odio et urna efficitur aliquam eu at nisi. Sed eget ante quis metus imperdiet venenatis.</p>\n\n<p>Mauris posuere enim nec augue imperdiet, eget tristique elit rhoncus. Proin ultricies libero quis arcu imperdiet tristique. Suspendisse feugiat, tellus a aliquam tincidunt, velit erat consectetur dolor, ac molestie felis nunc eget eros. Fusce sed gravida tellus. Nullam convallis libero sit amet ipsum euismod scelerisque. Nullam auctor enim enim, at hendrerit purus congue auctor. Nulla molestie ac sem id tincidunt. In at urna metus. Etiam rutrum erat in lectus ultrices aliquet. In consequat, tortor elementum gravida porttitor, elit lorem molestie justo, vestibulum pretium mi odio non nunc. Sed rhoncus tortor dolor. Vestibulum consectetur risus ut fringilla facilisis. Ut nibh quam, pulvinar a nisi suscipit, placerat tempor purus. Nam congue, turpis ac tristique scelerisque, risus est mattis leo, quis varius mauris elit vitae enim. Nunc at nulla est.</p>\n\n<p>Aliquam accumsan finibus dui. Ut ligula turpis, finibus et purus at, sodales ultrices nisl. Praesent ut risus nec neque efficitur condimentum. Ut elementum justo quis neque tincidunt congue in varius metus. Pellentesque id risus eu nisl rutrum ultricies sed et ex. Fusce mattis est non lectus sodales, at tempus augue auctor. Ut mauris ex, tristique vel hendrerit ut, consectetur eu purus. Nulla mollis est et tincidunt molestie. Aenean vitae dapibus elit.</p>\n\n<p>Cras efficitur diam eget lectus gravida, et rutrum neque pharetra. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Vestibulum at mauris turpis. Maecenas non massa vitae ligula condimentum egestas vitae sit amet ligula. Nulla ut semper est. In eget nunc ornare, venenatis leo non, faucibus leo. Mauris sed augue quis velit gravida facilisis vitae vitae risus. Curabitur sed erat sed leo tempor elementum non ut metus. Vestibulum mattis, velit quis tempor tincidunt, eros nunc mattis leo, ut volutpat mi velit vitae felis. Mauris euismod libero sed pellentesque fermentum. Nulla lacus est, tristique sit amet eros in, scelerisque dictum arcu. Vestibulum suscipit ex eu varius pretium. Donec a velit vitae tortor eleifend ultricies.</p>\n\n<p>Nunc ornare nec lorem at feugiat. Maecenas non porta nibh, vitae congue sapien. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed viverra risus vel erat mollis, in vulputate ipsum pretium. Mauris nec elit commodo, scelerisque nulla et, faucibus metus. In diam nibh, laoreet vel suscipit at, hendrerit non tortor. Sed sit amet varius felis. Vestibulum id diam in urna sollicitudin vestibulum vel et libero. Suspendisse tortor eros, varius a imperdiet eu, lacinia eget ex. Donec hendrerit pulvinar gravida. Quisque suscipit fringilla hendrerit. Nam non scelerisque dolor. Integer bibendum nec erat sit amet imperdiet. Suspendisse maximus tristique lacus, nec malesuada dui volutpat at.</p>\n', '2016-06-06 11:34:23', 1465212863, 58, 1, 'O mnie', 'Witajcie ziomeczki i ziomale! oto ja krol wszechswiata tego tamtego i kazdego o ktorym pomyslicie!', 'o mnie,autor');

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `postID` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `categoryID` int(11) NOT NULL,
  `content` text COLLATE utf8_polish_ci NOT NULL,
  `publishDate` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `publishDateStamp` int(11) NOT NULL,
  `promoImage` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `published` int(11) NOT NULL,
  `visited` int(11) NOT NULL,
  `seoTitle` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `seoDesc` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `seoKeywords` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`postID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=26 ;

--
-- Zrzut danych tabeli `posts`
--

INSERT INTO `posts` (`postID`, `title`, `alias`, `categoryID`, `content`, `publishDate`, `publishDateStamp`, `promoImage`, `published`, `visited`, `seoTitle`, `seoDesc`, `seoKeywords`) VALUES
(23, 'Pierwszy post', 'no-one', 1, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque et orci eget lectus gravida vestibulum. Vivamus semper mattis sollicitudin. Donec id sollicitudin nisl. Etiam in est odio. Quisque sit amet odio eget magna tempus dapibus vitae sed lacus. Aenean ut posuere ex. Maecenas et malesuada felis. Donec dapibus felis justo, in bibendum magna vulputate id. Nam sodales ut augue ut porttitor. Duis efficitur dui ut erat feugiat, vitae elementum mi ultricies. Pellentesque vehicula at dolor id sollicitudin.</p>\n\n<p>Proin pulvinar hendrerit felis ut fringilla. Aliquam molestie lobortis leo, euismod lacinia tellus sodales sed. Etiam rutrum mollis dui, sodales porta purus finibus at. Proin ut nulla sit amet ante lacinia posuere. Quisque at tempus lorem, a aliquet lorem. Suspendisse sed mollis eros, sed porttitor eros. Morbi auctor ullamcorper sagittis. Phasellus iaculis euismod arcu ut cursus. Aenean eget dictum enim. Mauris bibendum fermentum malesuada. Nunc in justo mollis, cursus elit eget, tempus nibh. Suspendisse potenti. Morbi id mauris magna. Suspendisse tincidunt aliquet facilisis. Integer consectetur, libero vitae lobortis aliquet, urna metus ultrices magna, ut sagittis turpis nibh sed urna.</p>\n\n<p>Cras sed risus sit amet erat blandit feugiat. Curabitur luctus lacus nec lobortis consectetur. Nullam sed orci ut neque interdum auctor. Maecenas sit amet diam vestibulum ipsum sagittis feugiat id eu justo. Nam ac sem vitae velit vehicula rhoncus a ac eros. Integer dictum massa quis justo sodales, non tempor nulla dictum. Maecenas interdum eu felis nec posuere. Donec auctor iaculis nulla, euismod maximus velit laoreet egestas. Fusce scelerisque sapien urna, quis malesuada nulla volutpat quis. Fusce accumsan vehicula mauris at euismod.</p>\n\n<p>Phasellus nisi nibh, ornare sed leo eget, placerat porta sapien. Vivamus fermentum metus eget tincidunt consectetur. Vivamus metus neque, sollicitudin a turpis sit amet, pharetra facilisis arcu. Morbi sagittis id mauris eget vestibulum. Etiam mollis vitae eros vel tincidunt. Fusce tincidunt convallis magna. Nunc id sem et nunc finibus tristique a eu urna. Nullam sollicitudin quis turpis id dignissim. Duis egestas consectetur est sit amet finibus. Sed nec mi quis ipsum varius dignissim at volutpat nisl. Nam commodo diam ut augue aliquam, sed mattis risus pellentesque. Cras suscipit suscipit massa nec efficitur.</p>\n\n<p>Pellentesque a neque tempus, rhoncus sem et, ullamcorper risus. Donec vitae lacinia nisi, in semper justo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce cursus, magna eget feugiat dapibus, ante enim semper libero, sit amet varius lacus nunc a metus. Duis molestie nec eros a vulputate. Nunc volutpat commodo massa ac venenatis. Sed pellentesque nisi et sem volutpat, id aliquam dui accumsan. Quisque at nunc est. Sed vestibulum porttitor erat, eget fermentum urna laoreet vitae. Duis pretium, libero at commodo sagittis, justo mi varius metus, et laoreet lectus leo sit amet magna. Mauris ullamcorper leo nunc, non feugiat metus vulputate ut. Phasellus mauris tellus, ultrices sit amet arcu sit amet, auctor pretium enim. Morbi tincidunt efficitur velit, at pellentesque nisl scelerisque a. Aliquam pharetra mi sem, et semper libero vehicula eget. Curabitur nisi purus, ullamcorper at lorem sed, mollis elementum leo.</p>\n', '2016-05-30 18:19:04', 0, 'resources/upload/DlAqwFxa3a.jpg', 1, 11, 'test', 'hoo', 'hmm'),
(24, 'drugi post', 'drugi-post', 1, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque et orci eget lectus gravida vestibulum. Vivamus semper mattis sollicitudin. Donec id sollicitudin nisl. Etiam in est odio. Quisque sit amet odio eget magna tempus dapibus vitae sed lacus. Aenean ut posuere ex. Maecenas et malesuada felis. Donec dapibus felis justo, in bibendum magna vulputate id. Nam sodales ut augue ut porttitor. Duis efficitur dui ut erat feugiat, vitae elementum mi ultricies. Pellentesque vehicula at dolor id sollicitudin.</p>\n\n<p>Proin pulvinar hendrerit felis ut fringilla. Aliquam molestie lobortis leo, euismod lacinia tellus sodales sed. Etiam rutrum mollis dui, sodales porta purus finibus at. Proin ut nulla sit amet ante lacinia posuere. Quisque at tempus lorem, a aliquet lorem. Suspendisse sed mollis eros, sed porttitor eros. Morbi auctor ullamcorper sagittis. Phasellus iaculis euismod arcu ut cursus. Aenean eget dictum enim. Mauris bibendum fermentum malesuada. Nunc in justo mollis, cursus elit eget, tempus nibh. Suspendisse potenti. Morbi id mauris magna. Suspendisse tincidunt aliquet facilisis. Integer consectetur, libero vitae lobortis aliquet, urna metus ultrices magna, ut sagittis turpis nibh sed urna.</p>\n\n<p>Cras sed risus sit amet erat blandit feugiat. Curabitur luctus lacus nec lobortis consectetur. Nullam sed orci ut neque interdum auctor. Maecenas sit amet diam vestibulum ipsum sagittis feugiat id eu justo. Nam ac sem vitae velit vehicula rhoncus a ac eros. Integer dictum massa quis justo sodales, non tempor nulla dictum. Maecenas interdum eu felis nec posuere. Donec auctor iaculis nulla, euismod maximus velit laoreet egestas. Fusce scelerisque sapien urna, quis malesuada nulla volutpat quis. Fusce accumsan vehicula mauris at euismod.</p>\n\n<p>Phasellus nisi nibh, ornare sed leo eget, placerat porta sapien. Vivamus fermentum metus eget tincidunt consectetur. Vivamus metus neque, sollicitudin a turpis sit amet, pharetra facilisis arcu. Morbi sagittis id mauris eget vestibulum. Etiam mollis vitae eros vel tincidunt. Fusce tincidunt convallis magna. Nunc id sem et nunc finibus tristique a eu urna. Nullam sollicitudin quis turpis id dignissim. Duis egestas consectetur est sit amet finibus. Sed nec mi quis ipsum varius dignissim at volutpat nisl. Nam commodo diam ut augue aliquam, sed mattis risus pellentesque. Cras suscipit suscipit massa nec efficitur.</p>\n\n<p>Pellentesque a neque tempus, rhoncus sem et, ullamcorper risus. Donec vitae lacinia nisi, in semper justo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce cursus, magna eget feugiat dapibus, ante enim semper libero, sit amet varius lacus nunc a metus. Duis molestie nec eros a vulputate. Nunc volutpat commodo massa ac venenatis. Sed pellentesque nisi et sem volutpat, id aliquam dui accumsan. Quisque at nunc est. Sed vestibulum porttitor erat, eget fermentum urna laoreet vitae. Duis pretium, libero at commodo sagittis, justo mi varius metus, et laoreet lectus leo sit amet magna. Mauris ullamcorper leo nunc, non feugiat metus vulputate ut. Phasellus mauris tellus, ultrices sit amet arcu sit amet, auctor pretium enim. Morbi tincidunt efficitur velit, at pellentesque nisl scelerisque a. Aliquam pharetra mi sem, et semper libero vehicula eget. Curabitur nisi purus, ullamcorper at lorem sed, mollis elementum leo.</p>\n', '2016-05-30 11:43:48', 0, 'resources/upload/VxXPSm1gXJ.jpg', 1, 3, '', '', ''),
(25, 'TRZECI poscik', 'no-three', 1, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque et orci eget lectus gravida vestibulum. Vivamus semper mattis sollicitudin. Donec id sollicitudin nisl. Etiam in est odio. Quisque sit amet odio eget magna tempus dapibus vitae sed lacus. Aenean ut posuere ex. Maecenas et malesuada felis. Donec dapibus felis justo, in bibendum magna vulputate id. Nam sodales ut augue ut porttitor. Duis efficitur dui ut erat feugiat, vitae elementum mi ultricies. Pellentesque vehicula at dolor id sollicitudin.</p>\n\n<p>Proin pulvinar hendrerit felis ut fringilla. Aliquam molestie lobortis leo, euismod lacinia tellus sodales sed. Etiam rutrum mollis dui, sodales porta purus finibus at. Proin ut nulla sit amet ante lacinia posuere. Quisque at tempus lorem, a aliquet lorem. Suspendisse sed mollis eros, sed porttitor eros. Morbi auctor ullamcorper sagittis. Phasellus iaculis euismod arcu ut cursus. Aenean eget dictum enim. Mauris bibendum fermentum malesuada. Nunc in justo mollis, cursus elit eget, tempus nibh. Suspendisse potenti. Morbi id mauris magna. Suspendisse tincidunt aliquet facilisis. Integer consectetur, libero vitae lobortis aliquet, urna metus ultrices magna, ut sagittis turpis nibh sed urna.</p>\n\n<p>Cras sed risus sit amet erat blandit feugiat. Curabitur luctus lacus nec lobortis consectetur. Nullam sed orci ut neque interdum auctor. Maecenas sit amet diam vestibulum ipsum sagittis feugiat id eu justo. Nam ac sem vitae velit vehicula rhoncus a ac eros. Integer dictum massa quis justo sodales, non tempor nulla dictum. Maecenas interdum eu felis nec posuere. Donec auctor iaculis nulla, euismod maximus velit laoreet egestas. Fusce scelerisque sapien urna, quis malesuada nulla volutpat quis. Fusce accumsan vehicula mauris at euismod.</p>\n\n<p>Phasellus nisi nibh, ornare sed leo eget, placerat porta sapien. Vivamus fermentum metus eget tincidunt consectetur. Vivamus metus neque, sollicitudin a turpis sit amet, pharetra facilisis arcu. Morbi sagittis id mauris eget vestibulum. Etiam mollis vitae eros vel tincidunt. Fusce tincidunt convallis magna. Nunc id sem et nunc finibus tristique a eu urna. Nullam sollicitudin quis turpis id dignissim. Duis egestas consectetur est sit amet finibus. Sed nec mi quis ipsum varius dignissim at volutpat nisl. Nam commodo diam ut augue aliquam, sed mattis risus pellentesque. Cras suscipit suscipit massa nec efficitur.</p>\n\n<p>Pellentesque a neque tempus, rhoncus sem et, ullamcorper risus. Donec vitae lacinia nisi, in semper justo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce cursus, magna eget feugiat dapibus, ante enim semper libero, sit amet varius lacus nunc a metus. Duis molestie nec eros a vulputate. Nunc volutpat commodo massa ac venenatis. Sed pellentesque nisi et sem volutpat, id aliquam dui accumsan. Quisque at nunc est. Sed vestibulum porttitor erat, eget fermentum urna laoreet vitae. Duis pretium, libero at commodo sagittis, justo mi varius metus, et laoreet lectus leo sit amet magna. Mauris ullamcorper leo nunc, non feugiat metus vulputate ut. Phasellus mauris tellus, ultrices sit amet arcu sit amet, auctor pretium enim. Morbi tincidunt efficitur velit, at pellentesque nisl scelerisque a. Aliquam pharetra mi sem, et semper libero vehicula eget. Curabitur nisi purus, ullamcorper at lorem sed, mollis elementum leo.</p>\n', '2016-05-30 11:44:43', 0, 'resources/upload/TJ26Sdiyfc.jpg', 1, 0, '', '', '');

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `settingID` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `keywords` varchar(255) NOT NULL,
  `siteTitle` varchar(255) NOT NULL,
  PRIMARY KEY (`settingID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin2 AUTO_INCREMENT=2 ;

--
-- Zrzut danych tabeli `settings`
--

INSERT INTO `settings` (`settingID`, `title`, `description`, `keywords`, `siteTitle`) VALUES
(1, 'Blog ng2', 'pierwsze podejscie ng2', 'angular, angular2', 'Blog ng2333');

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `userID` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(2555) COLLATE utf8_polish_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `admin` int(11) NOT NULL,
  `api_token` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `api_token_expired` int(11) NOT NULL,
  PRIMARY KEY (`userID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci AUTO_INCREMENT=2 ;

--
-- Zrzut danych tabeli `users`
--

INSERT INTO `users` (`userID`, `login`, `password`, `admin`, `api_token`, `api_token_expired`) VALUES
(1, 'admin', '$2y$10$rIFle9sTTSZf2jmgYX6z1uPmXsNJHbPROu/8E3qlBghx5f2g03BHS', 1, 'wJ2Di6Eqi7s0mxbWcOEnS6JBdgHQsunzmWkgsqVpdl4CBKOkmiQp7e8ItXtMfLKrmuJkub62tM6xGss4PHxM7t15KJYXrnLLJMgCtgwA3BosK2UsNknyc6yjXAV4J85B9HI8A55ja7qRsRlQ9HOyC0M5lhvEldAGP8g6MCrA9LcMjgH1QnDHasnY2uDlpTEHvx2pP53vTIHUDwbM09B3zKCk9t9HwYuW2FFFLRH9RbXZbc0BKCv6NnYHjaSk4au', 1465220017);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
